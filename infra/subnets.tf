resource "aws_subnet" "public-1-venkateswarlu" {

  vpc_id = aws_vpc.main.id

  cidr_block        = "145.45.0.0/18"
  availability_zone = "ap-south-1a"

  # required for eks .instances launched into the subnet should be assigned a public ip address
  map_public_ip_on_launch = true

  tags = {
    Name                        = "public-ap-south-1a-venkateswarlu"
    "kubernetes.io/cluster/eks" = "owned"
    "kubernetes.io/role/elb"    = 1
  }
}

resource "aws_subnet" "public-2-venkateswarlu" {

  vpc_id = aws_vpc.main.id

  cidr_block        = "145.45.64.0/18"
  availability_zone = "ap-south-1b"

  # required for eks .instances launched into the subnet should be assigned a public ip address
  map_public_ip_on_launch = true

  tags = {
    Name                        = "public-ap-south-1b-venkateswarlu"
    "kubernetes.io/cluster/eks" = "owned"
    "kubernetes.io/role/elb"    = 1
  }
}

resource "aws_subnet" "private-1-venkateswarlu" {

  vpc_id = aws_vpc.main.id

  cidr_block        = "145.45.128.0/18"
  availability_zone = "ap-south-1a"



  tags = {
    Name                              = "private-ap-south-1a-venkateswarlu"
    "kubernetes.io/cluster/eks"       = "owned"
    "kubernetes.io/role/internal-elb" = 1
  }
}

resource "aws_subnet" "private-2-venkateswarlu" {

  vpc_id = aws_vpc.main.id

  cidr_block        = "145.45.192.0/18"
  availability_zone = "ap-south-1b"



  tags = {
    Name                              = "private-ap-south-1b-venkateswarlu"
    "kubernetes.io/cluster/eks"       = "owned"
    "kubernetes.io/role/internal-elb" = 1
  }
}


