resource "aws_eks_cluster" "eks" {

  name = "eks-venkateswarlu"

  role_arn = "arn:aws:iam::382383803837:role/infra-bootcamp-eks-role"


  vpc_config {

    endpoint_private_access = false

    endpoint_public_access = true

    subnet_ids = [
      aws_subnet.public-1-venkateswarlu.id,
      aws_subnet.public-2-venkateswarlu.id,
      aws_subnet.private-1-venkateswarlu.id,
      aws_subnet.private-2-venkateswarlu.id

    ]

  }

}
