resource "aws_eip" "eip1" {

  #explicit dependency on the igw.
  depends_on = [
    aws_internet_gateway.main
  ]

  tags = {
    "Name" = "eip1-venkateswarlu"
  }
}

resource "aws_eip" "eip2" {

  #explicit dependency on the igw.
  depends_on = [
    aws_internet_gateway.main
  ]

  tags = {
    "Name" = "eip2-venkateswarlu"
  }
}
