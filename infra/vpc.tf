resource "aws_vpc" "main" {

  cidr_block       = "145.45.0.0/16"
  instance_tenancy = "default"

  # this two required by EKS. enable/disable dns support for vpc.
  enable_dns_support   = true
  enable_dns_hostnames = true

  enable_classiclink               = false
  assign_generated_ipv6_cidr_block = false


  tags = {
    Name = "main-venkateswarlu"
  }
}

output "vpc_id" {
  value       = aws_vpc.main.id
  description = "VPC id "

  sensitive = false
}
