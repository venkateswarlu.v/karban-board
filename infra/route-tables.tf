resource "aws_route_table" "public" {

  vpc_id = aws_vpc.main.id


  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = {
    "Name" = "public-venkateswarlu"
  }


}
resource "aws_route_table" "private-1" {

  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.ngw1.id
  }

  tags = {
    "Name" = "private1-venkateswarlu"
  }



}
resource "aws_route_table" "private-2" {

  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.ngw2.id
  }

  tags = {
    "Name" = "private2-venkateswarlu"
  }


}

