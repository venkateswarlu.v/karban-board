resource "aws_nat_gateway" "ngw1" {

  allocation_id = aws_eip.eip1.id

  subnet_id = aws_subnet.public-1-venkateswarlu.id

  tags = {
    "Name" = "ngw1-venkateswarlu"
  }

}

resource "aws_nat_gateway" "ngw2" {

  allocation_id = aws_eip.eip2.id

  subnet_id = aws_subnet.public-2-venkateswarlu.id

  tags = {
    "Name" = "ngw2-venkateswarlu"
  }

}
