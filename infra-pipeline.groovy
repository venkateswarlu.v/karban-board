pipeline {
    environment {
        registry = 'zero2onebase2/karbanboard'
        registryCredential = 'dockerhub'
        dockerImage = ''
    }
    agent any
    stages {
        stage('clone repo') {
            steps {
                git 'https://gitlab.com/venkateswarlu.v/karban-board'
            }
        }
        stage('intialise terraform') {
            steps {
                dir('infra')
                {
                    sh 'terraform init'
                }
            }
        }

        stage('Validate infra') {
            steps {
                dir('infra') {
                    sh 'terraform validate'
                }
            }
        }

        stage('Destroy infra') {
            steps {
                dir('infra') {
                    sh 'terraform destroy -auto-approve'
                }
            }
        }
    }
}
